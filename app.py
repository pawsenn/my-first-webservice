import web
# import xml.etree.ElementTree as ET
import lxml.etree as ET
from enum import Enum

# Generating routes
urls = (
    '/c=(.*)', 'get_temp_c',
    '/f=(.*)', 'get_temp_f',
    '/k=(.*)', 'get_temp_k',
)
app = web.application(urls, globals())

# Constants
K_C_CONSTANT = 273.15
F_C_CONSTANT = 32
C_F_MULTIPLY = 9 / 5


class Unit(Enum):
    C = "Celsius"
    F = "Fahrenheit"
    K = "Kelvin"


class Temperature:
    def __init__(self, temp: float, unit: Unit):
        match unit:
            case Unit.C:
                self.temp_c = temp
                self.temp_f = self.c_to_f(temp)
                self.temp_k = self.c_to_k(temp)
            case Unit.F:
                self.temp_f = temp
                self.temp_c = self.f_to_c(temp)
                self.temp_k = self.f_to_k(temp)
            case Unit.K:
                self.temp_k = temp
                self.temp_c = self.k_to_c(temp)
                self.temp_f = self.k_to_f(temp)

    def c_to_f(self, temp_c: float) -> float:
        return temp_c * C_F_MULTIPLY + F_C_CONSTANT

    def c_to_k(self, temp_c: float) -> float:
        return temp_c + K_C_CONSTANT

    def f_to_c(self, temp_f: float) -> float:
        return (temp_f - F_C_CONSTANT) / C_F_MULTIPLY

    def f_to_k(self, temp_f: float) -> float:
        return self.c_to_k(self.f_to_c(temp_f))

    def k_to_c(self, temp_k: float) -> float:
        return temp_k - K_C_CONSTANT

    def k_to_f(self, temp_k: float) -> float:
        return self.c_to_f(self.k_to_c(temp_k))

    def __str__(self):
        return f"<?xml version='1.0' encoding='UTF-8'?>\n" \
               f"<temperature>\n" \
               f"  <celsius>{self.temp_c}</celsius>\n" \
               f"  <fahrenheit>{self.temp_f}</fahrenheit>\n" \
               f"  <kelvin>{self.temp_k}</kelvin>\n" \
               f"</temperature>"


def export_text_to_file(filename, text):
    with open(filename, "wt") as f:
        print(text, file=f)


def convert(xml_filename: str, xsl_filename: str) -> str:
    dom = ET.parse(xml_filename)
    xslt = ET.parse(xsl_filename)
    transform = ET.XSLT(xslt)
    newdom = transform(dom)
    return ET.tostring(newdom, pretty_print=True).decode('utf-8')


class get_temp_c:
    def GET(self, temp_c):
        try:
            temperature = Temperature(float(temp_c), Unit.C)

            export_text_to_file("file.xml", str(temperature))
            output = convert('file.xml', 'file.xsl')
        except ValueError as e:
            output = f"<temperature>\n" \
                     f"  <error>{str(e)}</error>\n" \
                     f"</temperature>"
        return output


class get_temp_f:
    def GET(self, temp_f):
        try:
            temperature = Temperature(float(temp_f), Unit.F)
            output = str(temperature)
        except ValueError:
            output = f"<temperature>\n" \
                     f"  <error>{str(ValueError)}</error>\n" \
                     f"</temperature>"
        return output


class get_temp_k:
    def GET(self, temp_k):
        try:
            temperature = Temperature(float(temp_k), Unit.F)
            output = str(temperature)
        except ValueError:
            output = f"<temperature>\n" \
                     f"  <error>{str(ValueError)}</error>\n" \
                     f"</temperature>"
        return output


if __name__ == "__main__":
    app.run()
