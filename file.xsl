<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <style>
    html * { font-family: Arial !important; }
   </style>
  </head>
  <body>
   <h2>Temperature conversion</h2>
   <table border="1">
    <tr bgcolor="#337aff">
     <th>Unit</th>
     <th>Value</th>
    </tr>

    <xsl:for-each select="temperature">
     <tr>
      <td>Celsius</td>
      <td><xsl:value-of select="celsius"/></td>
     </tr>
     <tr>
      <td>Fahrenheit</td>
      <td><xsl:value-of select="fahrenheit"/></td>
     </tr>
     <tr>
      <td>Kelvin</td>
      <td><xsl:value-of select="kelvin"/></td>
     </tr>
    </xsl:for-each>
   </table>
  </body>
 </html>
</xsl:template>
</xsl:stylesheet>